package ru.rastorguev.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

@RequiredArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Getter
    @Nullable
    private User currentUser = null;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    @Override
    public IRepository<User> getRepository() {
        return userRepository;
    }

    @Override
    public void userRegistry(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        userRepository.userRegistry(login, password);
    }

    @Override
    public void adminRegistry(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (isExistByLogin(login)) return;
        userRepository.adminRegistry(login, password);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isExistByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isExistByLogin(login);
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public boolean isRolesAllowed(@Nullable final Role... roles) {
        if (roles == null) return false;
        if (currentUser == null || currentUser.getRole() == null) return false;
        @Nullable final List<Role> listOfRoles = Arrays.asList(roles);
        return listOfRoles.contains(currentUser.getRole());
    }

    @Override
    public void logIn(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        if (user.getPassHash().equals(mdHashCode(password))) {
            currentUser = user;
        }
    }

    @Override
    public void logOut() {
        currentUser = null;
    }
}