package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IService;
import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.Collection;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public abstract IRepository<E> getRepository();

    @NotNull
    @Override
    public Collection<E> findAll() {
        return getRepository().findAll();
    }


    @Override
    public E findOne(final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        return (E) getRepository().findOne(entityId);
    }

    @Override
    public E persist(final E entity) {
        if (entity == null) return null;
        return (E) getRepository().persist(entity);
    }

    @Override
    public E merge(final E entity) {
        if (entity == null) return null;
        return (E) getRepository().merge(entity);
    }

    @Override
    public E remove(final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        return (E) getRepository().remove(entityId);
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }
}