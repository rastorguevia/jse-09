package ru.rastorguev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import static ru.rastorguev.tm.view.View.printAllProjectsForUserByCreationDate;
import static ru.rastorguev.tm.view.View.printAllProjectsForUserByStartDate;

public class ProjectListByStartDateCommand extends AbstractCommand {
    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "project_list_start";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all projects by start date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project list by start date");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUserByStartDate(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
