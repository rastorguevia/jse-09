package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.List;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class ProjectFindCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find project by part of title or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Find project");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        @NotNull final String userId = user.getId();
        System.out.println("Enter part of title or description");
        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        @NotNull final List<Project> projectListByWord = serviceLocator.getProjectService().findProjectsByInputAndUserId(input, userId);
        printProjectsByWord(projectListByWord);
        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}