package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task remove");
        System.out.println("Enter Project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int projectNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(projectNumber, user.getId());
        @NotNull final List<Task> taskListByProjectId = serviceLocator.getTaskService().taskListByProjectId(projectId);
        printTaskListByProjectId(taskListByProjectId);
        System.out.println("Enter Task ID");
        final int taskNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String taskId = serviceLocator.getTaskService().getTaskIdByNumber(taskNumber,taskListByProjectId);
        serviceLocator.getTaskService().remove(taskId);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}