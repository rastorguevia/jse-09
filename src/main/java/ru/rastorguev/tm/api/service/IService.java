package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IService<E extends AbstractEntity> {

    Collection<E> findAll();

    E findOne(String entityId);

    E persist(E entity);

    E merge(E entity);

    E remove(String entityId);

    void removeAll();
}
