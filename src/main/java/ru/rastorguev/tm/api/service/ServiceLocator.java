package ru.rastorguev.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IUserService getUserService();

    ITerminalService getTerminalService();

    ICommandService getCommandService();
}
