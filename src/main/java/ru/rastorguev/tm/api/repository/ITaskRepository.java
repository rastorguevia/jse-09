package ru.rastorguev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void removeTaskListByProjectId(final String projectId);

    List<Task> taskListByUserId (final String userId);

    List<Task> filteredTaskListByUserIdAndInput (final String userId, final String input);

    List<Task> taskListByProjectId (final String projectId);
}