package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.util.DateUtil.stringToDate;
import static ru.rastorguev.tm.view.View.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.command.system.AboutCommand;
import ru.rastorguev.tm.command.system.ExitCommand;
import ru.rastorguev.tm.command.system.HelpCommand;
import ru.rastorguev.tm.command.project.*;
import ru.rastorguev.tm.command.task.*;
import ru.rastorguev.tm.command.user.*;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.service.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandMap);

    public void init() throws Exception {
        initCommands();
        createDefaultUsers();


        //тестовые значения
        getUserService().logIn("user", "user123");

        Project project1 = new Project();
        project1.setName("project 1");
        project1.setUserId(getUserService().getCurrentUser().getId());
        project1.setDescription("sun");
        project1.setStartDate(stringToDate("23.10.1995"));

        Project project2 = new Project();
        project2.setName("project 2");
        project2.setUserId(getUserService().getCurrentUser().getId());
        project2.setStartDate(stringToDate("01.10.1995"));

        Project project3 = new Project();
        project3.setName("project 3");
        project3.setUserId(getUserService().getCurrentUser().getId());
        project3.setStartDate(stringToDate("22.10.1995"));

        getProjectService().persist(project1);
        getProjectService().persist(project2);
        getProjectService().persist(project3);

        Task task1 = new Task(project1.getId());
        task1.setUserId(getUserService().getCurrentUser().getId());
        task1.setProjectName(project1.getName());
        task1.setName("task 1 project 1");
        task1.setStartDate(stringToDate("22.10.2020"));

        Task task2 = new Task(project1.getId());
        task2.setUserId(getUserService().getCurrentUser().getId());
        task2.setProjectName(project1.getName());
        task2.setName("task 2 project 1 edited");
        task2.setDescription("car");
        task2.setStartDate(stringToDate("01.09.2020"));

        Task task3 = new Task(project2.getId());
        task3.setUserId(getUserService().getCurrentUser().getId());
        task3.setProjectName(project2.getName());
        task3.setName("task 1 project 2");

        getTaskService().persist(task1);
        getTaskService().persist(task2);
        getTaskService().persist(task3);



        launchConsole();
    }

    private void initCommands() throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.rastorguev.tm.command").getSubTypesOf(AbstractCommand.class);
        for (@NotNull Class<? extends AbstractCommand> commandClass : classes) {
            @NotNull final AbstractCommand command = commandClass.newInstance();
            command.setServiceLocator(this);
            commandMap.put(command.getName(), command);
        }
    }

    public void launchConsole() throws Exception {
        showWelcomeMsg();

        @Nullable String input = "";
        while (true) {
           try {
               input = terminalService.nextLine().toLowerCase();
               execute(input);
           } catch (Exception e) {
               e.printStackTrace();
           }
        }
    }

    private void execute(@Nullable final String input) throws Exception {
        if (input == null || input.isEmpty()) {
            showUnknownCommandMsg();
            return;
        }
        @Nullable final AbstractCommand command = commandMap.get(input);
        if (command == null) {
            showUnknownCommandMsg();
            return;
        }
        @NotNull final boolean secureCheck = !command.secure() || (command.secure() && userService.isAuth());
        @NotNull final boolean roleCheck = command.roles() == null ||
                (command.roles() != null && userService.isRolesAllowed(command.roles()));
        if (secureCheck && roleCheck) command.execute();
        else showAccessDeniedMsg();
    }

    private void createDefaultUsers() {
        userService.userRegistry("user", "user123");
        userService.adminRegistry("admin", "admin123");
    }
}