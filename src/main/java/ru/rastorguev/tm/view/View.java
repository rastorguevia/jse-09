package ru.rastorguev.tm.view;

import static ru.rastorguev.tm.comparator.EntityComparator.*;
import static ru.rastorguev.tm.util.DateUtil.*;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class View {

    public static void showWelcomeMsg() {
        System.out.println("* TASK MANAGER * \n" +
                "* help - show all commands * \n" +
                "* command case is not important *");
    }

    public static void showUnknownCommandMsg() {
        System.out.println("Unknown Command \n" +
                "try again");
    }

    public static void showAccessDeniedMsg() {
        System.out.println("Access Denied");
    }

    public static void printAllProjects(@Nullable final Collection<Project> projectCollection) {
        if (projectCollection == null) return;
        @NotNull final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectCollection);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public static void printAllProjectsForUser(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName());
        }
    }

    public static void printAllProjectsForUserByCreationDate(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        listOfProjects.sort(comparatorCreationDate);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName());
        }
    }

    public static void printAllProjectsForUserByStartDate(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        listOfProjects.sort(comparatorStartDate);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName()
                    + "\n Start date : " + dateFormatter.format(listOfProjects.get(i).getStartDate()));
        }
    }

    public static void printAllProjectsForUserByEndDate(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        listOfProjects.sort(comparatorEndDate);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName()
                    + "\n End date : " + dateFormatter.format(listOfProjects.get(i).getEndDate()));
        }
    }

    public static void printAllProjectsForUserByStatus(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        listOfProjects.sort(comparatorStatus);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName());
        }
    }

    public static void printProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("Project name: " + project.getName() +
                "\nProject description: " + project.getDescription() +
                "\nProject start date: " + dateFormatter.format(project.getStartDate()) +
                "\nProject end date: " + dateFormatter.format(project.getEndDate()) +
                "\nProject status: " + project.getStatus().getDisplayName());
    }

    public static void printProjectsByWord(@NotNull final List<Project> listOfProjects) {
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName()
                    + "  Status : " + listOfProjects.get(i).getStatus().getDisplayName() +
                    "\nProject description: " + listOfProjects.get(i).getDescription() +
                    "\nProject start date: " + dateFormatter.format(listOfProjects.get(i).getStartDate()) +
                    "\nProject end date: " + dateFormatter.format(listOfProjects.get(i).getEndDate()) + "\n");
        }
    }

    public static void printTaskListByProjectId(@NotNull final List<Task> taskListByProjectId){
        for (int i = 0; i < taskListByProjectId.size(); i++) {
            System.out.println((i + 1) + "." + taskListByProjectId.get(i).getName()
                    + "  Status : " +taskListByProjectId.get(i).getStatus().getDisplayName());
        }
    }

    public static void printTaskListByProjectIdByCreationDate(@NotNull final List<Task> taskListByProjectId) {
        taskListByProjectId.sort(comparatorCreationDate);
        for (int i = 0; i < taskListByProjectId.size(); i++) {
            System.out.println((i + 1) + "." + taskListByProjectId.get(i).getName()
                    + "  Status : " +taskListByProjectId.get(i).getStatus().getDisplayName());
        }
    }

    public static void printTaskListByProjectIdByStartDate(@NotNull final List<Task> taskListByProjectId) {
        taskListByProjectId.sort(comparatorStartDate);
        for (int i = 0; i < taskListByProjectId.size(); i++) {
            System.out.println((i + 1) + "." + taskListByProjectId.get(i).getName()
                    + "  Status : " +taskListByProjectId.get(i).getStatus().getDisplayName()
                    + "\n Start date : " + dateFormatter.format(taskListByProjectId.get(i).getStartDate()));
        }
    }

    public static void printTaskListByProjectIdByEndDate(@NotNull final List<Task> taskListByProjectId) {
        taskListByProjectId.sort(comparatorEndDate);
        for (int i = 0; i < taskListByProjectId.size(); i++) {
            System.out.println((i + 1) + "." + taskListByProjectId.get(i).getName()
                    + "  Status : " +taskListByProjectId.get(i).getStatus().getDisplayName()
                    + "\n End date : " + dateFormatter.format(taskListByProjectId.get(i).getEndDate()));
        }
    }

    public static void printTaskListByProjectIdByStatus(@NotNull final List<Task> taskListByProjectId) {
        taskListByProjectId.sort(comparatorStatus);
        for (int i = 0; i < taskListByProjectId.size(); i++) {
            System.out.println((i + 1) + "." + taskListByProjectId.get(i).getName()
                    + "  Status : " +taskListByProjectId.get(i).getStatus().getDisplayName());
        }
    }

    public static void printTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("Task name: " + task.getName() +
                "\nTask description: " + task.getDescription() +
                "\nTask start date: " + dateFormatter.format(task.getStartDate()) +
                "\nTask end date: " + dateFormatter.format(task.getEndDate()) +
                "\nTask status: " + task.getStatus().getDisplayName());
    }

    public static void printTasksByWord(@NotNull List<Task> listOfTasks) {
        for (@NotNull final Task task : listOfTasks) {
            @NotNull final String stringName = "Project name: ";
            @NotNull final String stringStatus = " Status : ";
            int length = task.getProjectName().length() + stringName.length()
                    + stringStatus.length() + task.getStatus().getDisplayName().length();

            printUnderline(length);
            System.out.println("\n" + stringName + task.getProjectName() + stringStatus + task.getStatus().getDisplayName());
            printUnderline(length);
            System.out.println("\nTask name: " + task.getName() +
                    "\nTask description: " + task.getDescription() +
                    "\nTask start date: " + dateFormatter.format(task.getStartDate()) +
                    "\nTask end date: " + dateFormatter.format(task.getEndDate()) +
                    "\nTask status: " + task.getStatus().getDisplayName());
        }
    }

    public static void printUnderline (int number) {
        if (number>0) {
            System.out.print("_");
            printUnderline(number - 1);
        }
    }


    public static void printUserProfile(@Nullable final User user) {
        if (user == null) return;
        System.out.println("User login: " + user.getLogin() +
                "\nUser role: " + user.getRole().getDisplayName());
    }
}